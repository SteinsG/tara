var gulp = require('gulp');
var browerSync = require('browser-sync');

gulp.task('browser-sync',function(){
    browerSync({
        server:{
            baseDir:"./"
        }
    });
});

gulp.task('default',['browser-sync'],function(){
    gulp.watch(['**/*.html'],browerSync.reload);
    gulp.watch(['**/css/*.css'],browerSync.reload);
    gulp.watch(['**/js/*.js'],browerSync.reload);
});